## Introduction
Datazoom is a high availability real-time data collection solution. This android framework project builds the framework file that will be distributed.

## Android Media player Collector library in your android application
The Android MediaPlayer framework allows access to the native media player included with the Android operating system. Datazoom’s NativeAndroidCollector facilitates native Android applications to send video playback events based on the configuration created in data-pipes.

## Usage of collector library

- You can find the usage instructions from the below url
- https://datazoom.atlassian.net/wiki/spaces/EN/pages/224428040/Media+Player+Android

## Demo Application

A demo application that shows the usage of this framework is available **[Here](https://gitlab.com/datazoom/mobile-android-group/mobile-android-native-demo)** .

## Credit
 - Shyam - Developed the base framework that can be used for all the players.
 - Roke Borgio - Developed the native media player collector framework
 - Veena B K - Implemented native media player sample application

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

